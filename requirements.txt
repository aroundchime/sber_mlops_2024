# local package
-e .

# external requirements
click
Sphinx
coverage
python-dotenv>=0.5.1
pandas
geopandas
shapely
dvc
dvc-s3

